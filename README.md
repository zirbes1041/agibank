**Desenvolvimento realizado:**

**API:**
1. Criei um CRUD para realizar a automação via rest.
2. Java Spring Boot
3. Maven

**Comandos para subir a aplicacao:**

* [ ] git clone https://gitlab.com/zirbes1041/agibank.git
* [ ] cd agibank
* [ ] cd api
* [ ] mvn clean install
* [ ] mvn spring-boot:run



**Automação dos testes:**

* Utilizei RestAssured para realizar o processo de automatização da API 
 1. Java
 2. Mongo
 3. Gradle

**Comandos da execução dos testes:**

* [ ] it clone https://gitlab.com/zirbes1041/agibank.git
* [ ] cd agibank
* [ ] cd restassured
* [ ] gradle test

