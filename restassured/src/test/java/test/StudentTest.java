package test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import io.restassured.http.ContentType;
import model.Student;
import repository.MongoDB;

public class StudentTest extends BasicTest {
	
	private MongoDB mongo = new MongoDB();
	
	Long id = null;
	
	@Test(priority=0)
	public void postStudents() {
		Student student = new Student("OPUS", "01739920058");
         
		id=given()
				.contentType(ContentType.JSON)
				.body(student).
		   when()
				.post("/students").
		   then()
				.statusCode(200)
				.body("name", is(student.getName()))
				.body("cpf", is(student.getCpf())).
		   and()
		        .contentType(ContentType.JSON).
		   extract()
		 		.jsonPath().getLong("id");
		
			mongo.MongoInsert(student.getName(), student.getCpf(), id, "POST");	
				
	}
	
	@Test(priority=3, dataProvider="students")
	public void postStudentsSet(Student student) {
		
		id=given()
				.contentType(ContentType.JSON)
				.body(student).
		   when()
				.post("/students").
		   then()
			    .statusCode(200).log().body().
		   and()
		   		.contentType(ContentType.JSON).
		   extract()
		 		.jsonPath().getLong("id");
			
		mongo.MongoInsert(student.getName(), student.getCpf(), id, "POST");	
	}
	
	@Test(priority=1)
	public void getStudents() {
		
		given()
			.pathParam("id", id).
		when()
			.get("/students/{id}").
		then()
			.statusCode(200).log().body().
		and()
	   		.contentType(ContentType.JSON)	
			;
		
		 mongo.MongoInsert(null, null, id, "GET");
	}
	
	@Test(priority=20)
	public void updateStudents() {
		Student studentToUpdate = new Student("Igor Xavier", "94919778066");
		
		given()
			.contentType(ContentType.JSON)
			.pathParam("id", id)
			.body(studentToUpdate).
		when()
			.put("/students/{id}").
		then()
			.statusCode(204);
		
		mongo.MongoInsert(studentToUpdate.getName(), studentToUpdate.getCpf(), id, "PUT");	
		
		given()
			.pathParam("id", id).
		when()
			.get("/students/{id}").
		then()
			.body("name", is(studentToUpdate.getName()))
			.body("cpf", is(studentToUpdate.getCpf()))
			.log().body();
		
		mongo.MongoInsert(studentToUpdate.getName(), studentToUpdate.getCpf(), id, "GET");	
	}
	
	@Test(priority=25, dependsOnMethods="updateStudents")
	public void deleteStudents() {
		
		given()
			.pathParam("id", id).
		when()
			.delete("/students/{id}").
		then()
			.statusCode(200);
		
		 mongo.MongoInsert(null, null, id, "DELETE");
		
		given()
			.pathParam("id", id).
		when()
			.get("/students/{id}").
		then()
			.statusCode(500);
		
		mongo.MongoInsert(null, null, id, "GET");
		
	}
	
	@DataProvider(name = "students")
	public Object[][] createData() {
		 return new Object[][] {
		   { new Student("Gilberto", "41020685034") },
		   { new Student("Amanda", "22109483040") },
		   { new Student("Patricia", "70713080043") },
		   { new Student("Lise", "47623794094") },
		   { new Student("Maicon", "90026437058") },
		   { new Student("Everton", "87701006087") },
		   { new Student("Geromel", "18013476030") },
		   { new Student("Paulo", "42936092027") },
		   { new Student("Beatriz", "48695235076") },
		 };
	}
	
	//Testes de excecao
	
	//Test POST - Sem informar body
	@Test(priority=0)
	public void postStudentsSemBody() {
         
		given()
				.contentType(ContentType.JSON).
		when()
				.post("/students").
		then()
				.statusCode(400);
		
			mongo.MongoInsert("Sem Body", null, null, "POST");	
			
	}
	
	//Test GET - informando id invalido
	@Test(priority=1)
	public void getStudentsIdInvalido() {
		id = 9999L;
		given()
			.pathParam("id", id).
		when()
			.get("/students/{id}").
		then()
			.statusCode(500).log().body();
		
		 mongo.MongoInsert(null, null, id, "GET");
	}
}
